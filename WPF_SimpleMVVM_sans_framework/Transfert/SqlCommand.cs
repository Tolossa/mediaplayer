﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace WpfMvvmSample.Transfert
{
    class SqlCommand
    {
        public void executeSqlCommand(string commande)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=MediaPlayerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = commande;
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void addBlobToDB(CloudBlockBlob blob, Stream file, string titre, string pathFile)
        {
            FileInfo yourfile = new FileInfo(pathFile);
            string url = "http" + blob.Uri.ToString().Substring(5);
            var cmd = "INSERT into media (Title, Length, Format, UploadDate, url) values ('" + titre + "', '"
                                                                                                    + yourfile.Length / 1000000 + "', '"
                                                                                                    + System.IO.Path.GetExtension(pathFile) + "', '"
                                                                                                    + DateTime.Now.ToString("yyyy-M-d hh:mm:ss") + "', '"
                                                                                                    + url + "')";
            executeSqlCommand(cmd);
        }
    }
}
