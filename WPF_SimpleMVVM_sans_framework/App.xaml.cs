﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using WpfMvvmSample.View;

namespace WpfMvvmSample
{
    /// <summary>
    /// Voici la classe générale de l'application
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // au démarrage de l'application on lance cette vue par défaut
            VoirContactView win = new VoirContactView();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            win.Show();
        }
    }
}
