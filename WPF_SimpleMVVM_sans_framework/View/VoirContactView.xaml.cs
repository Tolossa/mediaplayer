﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfMvvmSample.ViewModel;
using WpfMvvmSample.ViewModel.Interface;
using WpfMvvmSample.Model.Classes;
using WpfMvvmSample.Model.Services;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using WpfMvvmSample.Transfert;

namespace WpfMvvmSample.View
{
    /// <summary>
    /// Voici la vue !
    /// Elle ne sert qu'à afficher les données en utilisant la puissance du binding
    /// C'est les données du view model qui vont être bindée au DataContext de cette vue
    /// </summary>
    public partial class VoirContactView : Window
    {
        public VoirContactView()
        {
            InitializeComponent();
            InitializeListView();
            // plus le code behind de cette vue reste vide le mieux c'est !
        }

        private void ckBoxChangeTitre_Checked(object sender, RoutedEventArgs e)
        {
            if (ckBoxChangeTitre.IsChecked == true)
            {
                nouveauTitreName.IsEnabled = true;
            }
            else
            {
                nouveauTitreName.IsEnabled = false;
            }
        }

        public void AddNewTitle()
        {
            if (listView.SelectedItems.Count > 0)
            {
                String str = listView.SelectedItems[0].ToString();
                String str2 = str.Substring(str.IndexOf(',') + 11);
                String titre = str2.Substring(0, str2.IndexOf(',') - 1);
                String Id = Regex.Match(listView.SelectedItems[0].ToString(), @"\d+").Value;

                var cmd = "UPDATE Media SET Title = ('" + newTitle.Text + "') WHERE Id = " + Id;
                WpfMvvmSample.Transfert.SqlCommand querry = new WpfMvvmSample.Transfert.SqlCommand();
                querry.executeSqlCommand(cmd);
                InitializeListView();
            }
        }

        private void InitializeListView()
        {
            listView.Items.Clear();
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=MediaPlayerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter("Select * from media order by Id asc", conn);

            DataSet ds = new DataSet();

            da.Fill(ds, "Media");

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                listView.Items.Add(new
                {
                    b_id = row["Id"].ToString(),
                    b_title = row["Title"].ToString(),
                    b_duree = row["Length"].ToString(),
                    b_format = row["Format"].ToString(),
                    b_date = row["UploadDate"].ToString(),
                    b_url = row["url"].ToString()
                });
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            InitializeListView();
        }
    }
}
