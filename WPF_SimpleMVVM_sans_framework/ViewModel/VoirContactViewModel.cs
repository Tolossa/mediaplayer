﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media;
using System.Linq.Expressions;
using WpfMvvmSample.Model;
using WpfMvvmSample.ViewModel.Interface;
using WpfMvvmSample.FrameworkMvvm;
using System.Windows.Input;
using WpfMvvmSample.Model.Classes;
using WpfMvvmSample.Model.Services;
using System.Windows;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using WpfMvvmSample.Transfert;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;

namespace WpfMvvmSample.ViewModel
{
    /// <summary>
    /// Voici le view model !
    /// C'est lui qui fait la liaison entre la vue et le modèle
    /// </summary>
    public class VoirContactViewModel : ObservableObject, IVoirContactViewModel
    {
        private VoirContactViewModel winparent;
        BlobTransfer transfer;
        WpfMvvmSample.Transfert.SqlCommand Sql;
        bool pathCheck = false;
        /// <summary>
        /// ID unique du contact
        /// </summary>
        private int? _ID = null;
        public int? ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                OnPropertyChanged("ID");
            }
        }
        
        /// <summary>
        /// Nom de famille du contact
        /// </summary>
        private string _nom = null;
        public string Nom
        {
            get { return _nom; }
            set
            {
                _nom = value;
                OnPropertyChanged("Nom");
            }
        }

        /// <summary>
        /// Prenom du contact
        /// </summary>
        private string _prenom = null;
        public string Prenom
        {
            get { return _prenom; }
            set
            {
                _prenom = value;
                OnPropertyChanged("Prenom");
            }
        }

        /// <summary>
        /// Age du contact
        /// Booléen nullable à trois états (null, true ou false)
        /// </summary>
        private int? _age = null;
        public int? Age
        {
            get { return _age; }
            set
            {
                _age = value;
                OnPropertyChanged("Age");
            }
        }

        /// <summary>
        /// Sexe du contact
        /// Booléen nullable à trois états (null, true ou false)
        /// </summary>
        private bool? _homme = null;
        public bool? Homme
        {
            get { return _homme; }
            set
            {
                _homme = value;
                OnPropertyChanged("Homme");
            }
        }

        private string _fileToUpload = null;
        public string fileToUpload
        {
            get { return _fileToUpload; }
            set
            {
                _fileToUpload = value;
                OnPropertyChanged("fileToUpload");
            }
        }

        private string _nouveauTitre = null;
        public string nouveauTitre
        {
            get { return _nouveauTitre; }
            set
            {
                _nouveauTitre = value;
                OnPropertyChanged("nouveauTitre");
            }
        }

        private string _titreToChange = null;
        public string TitreToChange
        {
            get { return _titreToChange; }
            set
            {
                _titreToChange = value;
                OnPropertyChanged("TitreToChange");
            }
        }

        private bool _isSelected;
        public bool isChecked
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected = value) return;

                _isSelected = value;
                OnPropertyChanged("isChecked");
            }
        }
        public string _textProgressBar;
        public string textProgressBar
        {
            get { return _textProgressBar; }
            set
            {
                _textProgressBar = value;
                OnPropertyChanged("textProgressBar");
            }
        }

        public int _progressValue = 0;
        public int ProgressValue
        {
            get { return _progressValue; }
            set
            {
                _progressValue = value;
                OnPropertyChanged("ProgressValue");
            }
        }
        /// <summary>
        /// Définition de la commande permettant de charger le contact
        /// </summary>
        public ICommand ChargerContactCommand { get; private set; }
        public ICommand ChooseFileToUpload { get; private set; }
        public ICommand ChargerNouveauTitre { get; private set; }
        public ICommand UploadFileToAzure { get; private set; }
        public ICommand ChangeTitle { get; private set; }
        /// <summary>
        /// Constructeur
        /// </summary>
        public VoirContactViewModel()
        {
            // on associe la commande ChargerContactCommand à la méthode ChargeContact
            ChargerContactCommand = new RelayCommand(ChargeContact);
            ChooseFileToUpload = new RelayCommand(chooseFileFromFilePicker);
            ChargerNouveauTitre = new RelayCommand(ChargeTitre);
            UploadFileToAzure = new RelayCommand(uploadFileToAzure);
        }



        /// <summary>
        /// Méthode pour charger un contact depuis le model (service contacts)
        /// On accède au service de façon très simple par un new, pas d'IOC ici...
        /// </summary>
        /// 
 
        public void ChargeTitre()
        {
            ServiceTextBox service = new ServiceTextBox();
            MyTextBox textBox = service.ChagerNouveauTitre(null);
            this.nouveauTitre = textBox.nouveauTitre;
        }
        public void chooseFileFromFilePicker()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            Nullable<bool> result = dlg.ShowDialog();
            ServiceTextBox service = new ServiceTextBox();
            
            if (result == true)
            {
                MessageBox.Show("result = true");
                MyTextBox textBox = service.ChargerFileToUpload(dlg.FileName);
                this.fileToUpload = textBox.fileToUpload;
            }
        }

        public void uploadFileToAzure()
        {
            MessageBox.Show("Debut de la procedure d'upload en cour");
            string accountName = "epitechmediaplayer";
            string accountKey = "Y0LdsthpOBBA5MOLWCw5nb8JkNcpZHF+3n40inj+5PC6or4spB+669VDqTdLXjO4qyoTnZSKb8Bryl3G6amsZA==";
            try
            {
                StorageCredentials creds = new StorageCredentials(accountName, accountKey);
                CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer sampleContainer = client.GetContainerReference("samples");
                sampleContainer.CreateIfNotExists();
                var permissions = sampleContainer.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                sampleContainer.SetPermissions(permissions);

                if (fileToUpload != null)
                {
                    CloudBlockBlob blob;
                    if (isChecked == true)
                        blob = sampleContainer.GetBlockBlobReference(nouveauTitre);
                    else
                    {
                        blob = sampleContainer.GetBlockBlobReference(System.IO.Path.GetFileName(fileToUpload));
                        nouveauTitre = System.IO.Path.GetFileName(fileToUpload);
                    }
                    using (Stream file = System.IO.File.OpenRead(fileToUpload))
                    {

                        transfer = new BlobTransfer();
                        Sql = new WpfMvvmSample.Transfert.SqlCommand();
                        transfer.TransferProgressChanged += transfer_TransferProgressChanged;
                        transfer.TransferCompleted += transfer_TransferCompleted;
                        transfer.UploadBlobAsync(blob, fileToUpload);
                        Sql.addBlobToDB(blob, file, nouveauTitre, fileToUpload);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR : " + ex);
            }
        }

        void transfer_TransferCompleted(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("Download termine");
        }

        void transfer_TransferProgressChanged(object sender, BlobTransfer.BlobTransferProgressChangedEventArgs e)
        {
            ProgressValue = e.ProgressPercentage;
            textProgressBar = e.ProgressPercentage + "%";
        }

        private void ChargeContact()
        {
            ServiceContact service = new ServiceContact();
            Contact Contact = service.Charger();

            if (Contact != null)
            {
                this.ID = Contact.ID;
                this.Nom = Contact.Nom;
                this.Prenom = Contact.Prenom;
                this.Age = Contact.Age;
                this.Homme = Contact.Homme;
            }
        }
    }
}
