﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfMvvmSample.Model.Classes;

namespace WpfMvvmSample.Model.Services
{
    public class ServiceTextBox
    {
        public MyTextBox ChargerFileToUpload(string str)
        {
            return new MyTextBox { fileToUpload = str };
        }
        public MyTextBox ChagerNouveauTitre(string str)
        {
            return new MyTextBox { nouveauTitre = str };
        }
    }
}
