﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebMediaPlayer;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Diagnostics;

namespace WebMediaPlayer.Controllers
{
    public class MediaController : Controller
    {
        private MediaPlayerDBEntities db = new MediaPlayerDBEntities();

        // GET: Media
        public ActionResult Index(string sortBy, string search)
        {
            var movies = from m in this.db.Media
                         select m;
            return this.View(movies);
        }

        // GET: Media/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Media media = this.db.Media.Find(id);
            if (media == null)
            {
                return this.HttpNotFound();
            }
            return this.View(media);
        }

        // GET: Media/Create
        public ActionResult Create()
        {
            return this.View();
        }

        // POST: Media/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Length,Format,UploadDate")] Media media)
        {
            if (ModelState.IsValid)
            {
                this.db.Media.Add(media);
                this.db.SaveChanges();
                return this.RedirectToAction("Index");
            }

            return this.View(media);
        }

        // GET: Media/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Media media = this.db.Media.Find(id);
            if (media == null)
            {
                return this.HttpNotFound();
            }
            return this.View(media);
        }

        // POST: Media/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Length,Format,UploadDate")] Media media)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(media).State = EntityState.Modified;
                this.db.SaveChanges();
                return this.RedirectToAction("Index");
            }
            return this.View(media);
        }

        // GET: Media/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Media media = this.db.Media.Find(id);
            if (media == null)
            {
                return this.HttpNotFound();
            }
            return this.View(media);
        }

        // POST: Media/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Media media = this.db.Media.Find(id);
            this.db.Media.Remove(media);
            this.db.SaveChanges();
            return this.RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
