﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaPlayerWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CenterWindowOnScreen();
            WindowsMediaPlayer win = new WindowsMediaPlayer();
            win.SetParent(this);
            win.CenterWindowOnScreen();
            this.Hide();
            win.Show();
        }
        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private void ValidateConnexion(object sender, RoutedEventArgs e)
        {
            if (LoginBox.Text == "neyron_b" && PasswordBox.Text == "0123" || CheatBox.IsChecked == true)
            {
                this.Hide();
                WindowsMediaPlayer win = new WindowsMediaPlayer();
                win.SetParent(this);
                win.CenterWindowOnScreen();
                win.Show();
            }
            else
            {
                MessageBox.Show("Combinaison Login-passowrd invalide, reessayez");
                LoginBox.Text = "";
                PasswordBox.Text = "";
                Player play = new Player();
                play.Show();
            }
        }

    }
}
