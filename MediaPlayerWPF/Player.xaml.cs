﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MediaPlayerWPF
{
    /// <summary>
    /// Interaction logic for Player.xaml
    /// </summary>

    public partial class Player : Window
    {
        bool fullScreen = false;
        double baseHeight;
        double baseWidth;

        public Player()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
            mePlayer.Play();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (mePlayer.Source != null)
            {
                if (mePlayer.NaturalDuration.HasTimeSpan)
                    lblStatus.Content = String.Format("{0} / {1}", mePlayer.Position.ToString(@"mm\:ss"), mePlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
            }
            else
                lblStatus.Content = "No file selected...";
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Play();
        }

        private void btnFullScreen_click(object sender, RoutedEventArgs e)
        {
            if (!fullScreen)
            {
                fullScreen = true;
                this.Background = new SolidColorBrush(Colors.Black);
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
                baseHeight = mePlayer.Height;
                baseWidth = mePlayer.Width;
                mePlayer.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
                mePlayer.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            }
            else
            {
                fullScreen = false;
                this.Background = new SolidColorBrush(Colors.White);
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.WindowState = WindowState.Normal;
                mePlayer.Height = baseHeight;
                mePlayer.Width = baseWidth;
            }
        }
        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Pause();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Stop();
        }

        internal string getSource()
        {
            return mePlayer.Source.ToString();
        }

        internal void setSource(string url)
        {
            mePlayer.Source = new Uri(url);
        }
    }
}
