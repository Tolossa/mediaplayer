﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Collections.ObjectModel;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;
using BlobTransferUI;
using WPFFolderBrowser;

namespace MediaPlayerWPF
{
    public partial class WindowsMediaPlayer : Window
    {
        private MainWindow winparent;
        BlobTransfer transfer;
        bool pathCheck = false;
        public WindowsMediaPlayer()
        {
            InitializeComponent();
            InitializeListView();
        }

        public void SetParent(MainWindow win)
        {
            this.winparent = win;
        }

        internal void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }
        void CloseMediaPlayer(object sender, CancelEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void chooseFileButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                fileToUpload.Text = filename;
            }
        }

        private void uploadFileToAzure(object sender, RoutedEventArgs e)
        {
            string accountName = "epitechmediaplayer";
            string accountKey = "Y0LdsthpOBBA5MOLWCw5nb8JkNcpZHF+3n40inj+5PC6or4spB+669VDqTdLXjO4qyoTnZSKb8Bryl3G6amsZA==";
            try
            {
                StorageCredentials creds = new StorageCredentials(accountName, accountKey);
                CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer sampleContainer = client.GetContainerReference("samples");
                sampleContainer.CreateIfNotExists();
                var permissions = sampleContainer.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                sampleContainer.SetPermissions(permissions);

                if (fileToUpload.Text != null)
                {
                    CloudBlockBlob blob;
                    if (ckBoxChangeTitre.IsChecked == true)
                        blob = sampleContainer.GetBlockBlobReference(nouveauTitre.Text);
                    else
                    {
                        blob = sampleContainer.GetBlockBlobReference(System.IO.Path.GetFileName(fileToUpload.Text));
                        nouveauTitre.Text = System.IO.Path.GetFileName(fileToUpload.Text);
                    }
                    using (Stream file = System.IO.File.OpenRead(fileToUpload.Text))
                    {
                        transfer = new BlobTransfer();
                        progressBar.Maximum = 200;
                        transfer.TransferProgressChanged += transfer_TransferProgressChanged;
                        transfer.TransferCompleted += transfer_TransferCompleted;
                        transfer.UploadBlobAsync(blob, fileToUpload.Text);
                        addBlobToDB(blob, file);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.ToString());
            }
        }

 
        private void addBlobToDB(CloudBlockBlob blob, Stream file)
        {
            FileInfo yourfile = new FileInfo(fileToUpload.Text);
            string url = "http" + blob.Uri.ToString().Substring(5);
            var cmd = "INSERT into media (Title, Length, Format, UploadDate, url) values ('" + nouveauTitre.Text + "', '"
                                                                                                    + yourfile.Length / 1000000 + "', '" 
                                                                                                    + System.IO.Path.GetExtension(fileToUpload.Text) + "', '"
                                                                                                    + DateTime.Now.ToString("yyyy-M-d hh:mm:ss") + "', '" 
                                                                                                    + url + "')";
            executeSqlCommand(cmd);
        }

        private void InitializeListView()
        {
            listView.Items.Clear();
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=MediaPlayerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter("Select * from media order by Id asc", conn);

            DataSet ds = new DataSet();

            da.Fill(ds, "Media");

            foreach ( DataRow row in ds.Tables[0].Rows)
            {
                listView.Items.Add(new
                {
                    b_id = row["Id"].ToString(),
                    b_title = row["Title"].ToString(),
                    b_duree = row["Length"].ToString(),
                    b_format = row["Format"].ToString(),
                    b_date = row["UploadDate"].ToString(),
                    b_url = row["url"].ToString()
                });
            }
        }

        private void layoutUpdated_handler(object sender, EventArgs e)
        {
            wrap.Width = this.ActualWidth;
            listView.Width = this.ActualWidth;
        }

        private void EnabledChangeTitre(object sender, System.Windows.RoutedEventArgs e)
        {
            if (ckBoxChangeTitre.IsChecked == true)
            {
                nouveauTitre.IsEnabled = true;
            }
            else
            {
                nouveauTitre.IsEnabled = false;
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {
                MessageBox.Show("you clicked " + listView.SelectedItems[0].ToString());
            }
            else
                MessageBox.Show("Please select an item");
        }

        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {
                String str = listView.SelectedItems[0].ToString();
                String str2 = str.Substring(str.IndexOf(',') + 11);
                String titre = str2.Substring(0, str2.IndexOf(',') - 1);
                String Id = Regex.Match(listView.SelectedItems[0].ToString(), @"\d+").Value;
 
                var cmd = "UPDATE Media SET Title = ('" + newTitle.Text + "') WHERE Id = " + Id;
                executeSqlCommand(cmd);
                InitializeListView();
            }
        }

        private void executeSqlCommand(string commande)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=MediaPlayerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = commande;
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        private void btnUpdate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            InitializeListView();
        }

         private void Button_Click_3(object sender, System.Windows.RoutedEventArgs e)
        {
            transfer.CancelAsync();
        }

        void transfer_TransferCompleted(object sender, AsyncCompletedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Completed. Cancelled = " + e.Cancelled);
        }

        void transfer_TransferProgressChanged(object sender, BlobTransfer.BlobTransferProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            if (progressBar.Maximum == 200)
                textProgresseBar.Text = (e.ProgressPercentage / 2) + "%";
            else
                textProgresseBar.Text = e.ProgressPercentage + "%";
            if (e.ProgressPercentage == progressBar.Maximum)
                MessageBox.Show(this, "Operation terminee.",
                 "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnSuppr_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {

                String str = listView.SelectedItems[0].ToString();
                String str2 = str.Substring(str.IndexOf(',') + 11);
                String titre = str2.Substring(0, str2.IndexOf(',') - 1);
                String Id = Regex.Match(listView.SelectedItems[0].ToString(), @"\d+").Value;
                if (MessageBox.Show("Etes vous sur de vouloir supprimer : " + titre.Trim() + " ?",
                "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.No)
                {
                    return;
                }
                CloudStorageAccount account = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials("epitechmediaplayer", "Y0LdsthpOBBA5MOLWCw5nb8JkNcpZHF+3n40inj+5PC6or4spB+669VDqTdLXjO4qyoTnZSKb8Bryl3G6amsZA=="), false);
                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.GetContainerReference("samples");
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(titre);
                if (blockBlob.Exists())
                    blockBlob.Delete();
                var cmd = "DELETE FROM Media WHERE Id='" + Id + "';";
                executeSqlCommand(cmd);
                MessageBox.Show(this, "Element supprime.",
                 "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
                InitializeListView();
            }
        }

        private void btnPlay_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {
                String str = listView.SelectedItems[0].ToString();
                String url = str.Substring(str.IndexOf("http"));
                url = url.Substring(0, url.Length - 2);
                Player play = new Player();
                play.setSource(url);
                var ext = System.IO.Path.GetExtension(play.getSource());
                if ((ext == ".mp3" ||
                    ext == ".wma" ||
                    ext == ".flac" ||
                    ext == ".wav" ||
                    ext == ".ac3" ||
                    ext == ".aac"))
                {
                    play.imageForAudio.Visibility = Visibility.Visible;
                }
                else
                    play.imageForAudio.Visibility = Visibility.Hidden;
                play.Show();

            }
            else
                MessageBox.Show("Select a media to play");
        }

        private void textChangeSearch(object sender, TextChangedEventArgs e)
        {
            listView.Items.Clear();
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=MediaPlayerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

            SqlDataAdapter da = new SqlDataAdapter("Select * from media WHERE Title like '%" + rechercheField.Text + "%' order by Id asc", conn);

            DataSet ds = new DataSet();

            da.Fill(ds, "Media");

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                listView.Items.Add(new
                {
                    b_id = row["Id"].ToString(),
                    b_title = row["Title"].ToString(),
                    b_duree = row["Length"].ToString(),
                    b_format = row["Format"].ToString(),
                    b_date = row["UploadDate"].ToString(),
                    b_url = row["url"].ToString()
                });
            }
        }

        private void buttonDownload(object sender, System.Windows.RoutedEventArgs e)
        {
            string accountName = "epitechmediaplayer";
            string accountKey = "Y0LdsthpOBBA5MOLWCw5nb8JkNcpZHF+3n40inj+5PC6or4spB+669VDqTdLXjO4qyoTnZSKb8Bryl3G6amsZA==";
            try
            {
                StorageCredentials creds = new StorageCredentials(accountName, accountKey);
                CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer sampleContainer = client.GetContainerReference("samples");
                String str = listView.SelectedItems[0].ToString();
                String str2 = str.Substring(str.IndexOf(',') + 11);
                String titre = str2.Substring(0, str2.IndexOf(',') - 1);
                titre = titre.Trim();
                CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(titre);

                transfer = new BlobTransfer();
                transfer.TransferProgressChanged += transfer_TransferProgressChanged;
                transfer.TransferCompleted += transfer_TransferCompleted;
                if (pathCheck)
                    transfer.DownloadBlobAsync(blob, pahtDownloadFile.Text + @"\" + titre);
                else
                    MessageBox.Show("Selectionnez un dossier de telechargement");
             }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.ToString());
            }
        }

        private void btnChooseFolder_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dlg = new WPFFolderBrowserDialog();
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                pathCheck = true;
                string filename = dlg.FileName;
                pahtDownloadFile.Text = filename;
            }
        }
    }
}